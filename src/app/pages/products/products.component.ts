// s04
import { Component, OnInit } from '@angular/core';

import { Product } from '@models/product';
import { ProductService } from '@services/product.service';

// add session service
import { SessionService } from '@services/session.service';

@Component({
    selector: 'app-products',
    templateUrl: './products.component.html',
    styleUrls: ['./products.component.css']
})
export class ProductsComponent implements OnInit {
    products: Product[] = [];
    isAdmin: boolean = false;

    constructor(
        private productService: ProductService,
        private sessionService: SessionService,
    ) {
        this.getProducts();
        // get isAdmin field to know if the user is admin
        this.isAdmin = sessionService.getIsAdmin();
    }

    ngOnInit(): void { }

    getProducts() {
        this.productService.get().subscribe((response: Product[]) => {
            this.products = response;
        });
    }

    onAddProductClick(): void { }

    deleteFromView(givenProduct: Product): void {
        this.products = this.products.filter(productEntry => productEntry !== givenProduct);
    }

}