// import { Component } from '@angular/core';

// s02
import { Component, OnInit } from '@angular/core';

// s04
import { Router } from '@angular/router';
import { SessionService } from '@services/session.service';

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.css']
})
export class NavbarComponent implements OnInit {
  // hasToken: boolean = false;
  // email: string;

  hasToken: boolean = (localStorage.getItem('token') !== null);
  email: String = localStorage.getItem('email')!;

  // constructor() { this.email = '';}
  constructor(
    private sessionService: SessionService,
    private router: Router
  ) {
    sessionService.hasToken.subscribe(hasToken => {
        this.hasToken = hasToken;
        this.email = this.sessionService.getEmail();
    });
  }

  ngOnInit(): void { }

  // logout(): void {
  //     console.log('Logout button has been clicked.');
  // }

  logout(): void {
    console.log('Logout button has been clicked.');
    this.sessionService.clear();
    this.router.navigate(['/login']);
  }
}
