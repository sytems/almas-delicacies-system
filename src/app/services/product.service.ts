//s04
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from '@environments/environment';
import { Observable } from 'rxjs';

import { Product } from '@models/product';

import { HttpHeaders } from '@angular/common/http';
import { SessionService } from './session.service';

@Injectable({
    providedIn: 'root'
})
export class ProductService {
    private baseUrl: string = environment.apiUrl + '/products';

    // for wildcard
    private httpHeaders: HttpHeaders = new HttpHeaders({
      'Authorization': `Bearer ${this.sessionService.getToken()}`
    });

    constructor(
        private http: HttpClient,

        // s04 add for session Service
        private sessionService: SessionService
    ) { }

    get(): Observable<Product[]> {
        return this.http.get<Product[]>(this.baseUrl);
    }

    getOne(id: number): Observable<Object> {
      return this.http.get<Product[]>(`${this.baseUrl}/${id}`);
    }

    add(product: Product): void { }

    update(product: Product): Observable<Object> {
      return this.http.put(this.baseUrl + `/${product.id}`, product, { headers: this.httpHeaders });
    }

    archive(id: number): Observable<Object> {
      return this.http.delete(this.baseUrl + `/${id}`, { headers: this.httpHeaders });
    }

}